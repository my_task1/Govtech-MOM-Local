######### STEP D. Use Custom Domain ##############################

resource "aws_api_gateway_domain_name" "this" {
  domain_name = var.api_domain_name
}

resource "aws_api_gateway_base_path_mapping" "this" {
  api_id      = aws_api_gateway_rest_api.this.id
  domain_name = aws_api_gateway_domain_name.this.domain_name
  stage_name  = var.stage
}
######### STEP D. Use Custom Domain ##############################


resource "aws_lambda_permission" "this" {
  #statement_id  = "AllowExecutionFromAPIGateway"
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/*"
}

# TO FILL IN resources, including but not limited to
# - aws_api_gateway_rest_api
# - aws_api_gateway_method
# - aws_api_gateway_deployment
############################### STEP C. Add API Key to API Gateway ############################
resource "aws_api_gateway_api_key" "this" {
  name    = "helloworld-localstack-test"
  enabled = true
}

resource "aws_api_gateway_usage_plan" "this" {
    name = "my_usage_plan"
  
}

resource "aws_api_gateway_usage_plan_key" "main" {
  key_id        = "${aws_api_gateway_api_key.this.id}"
  key_type      = "API_KEY"
  usage_plan_id = "${aws_api_gateway_usage_plan.this.id}"
}
############################### STEP C. Add API Key to API Gateway ############################


############################### STEP B. Create API Gateway ############################
resource "aws_api_gateway_rest_api" "this" {
  name        = var.name
  description = "Test"
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "this" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "ANY"
  authorization = "NONE"
}



resource "aws_api_gateway_integration" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_resource.this.id
  http_method = aws_api_gateway_method.this.http_method

  integration_http_method = "POST"
  type                   = "AWS_PROXY"
  uri                    = aws_lambda_function.this.invoke_arn
}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = "${aws_api_gateway_rest_api.this.id}"
  resource_id   = "${aws_api_gateway_rest_api.this.root_resource_id}"
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = "${aws_api_gateway_rest_api.this.id}"
  resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.this.invoke_arn}"
}

resource "aws_api_gateway_deployment" "this" {
  depends_on = [
    aws_api_gateway_method.this,
    aws_api_gateway_integration.this,
    aws_api_gateway_integration.lambda_root
  ]
  rest_api_id = aws_api_gateway_rest_api.this.id
  stage_name  = var.stage
}
############################### STEP B. Create API Gateway ############################
